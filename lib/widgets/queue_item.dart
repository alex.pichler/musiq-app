import 'package:flutter/material.dart';

import '../entities/song.dart';
import '../model/queue.dart';

class QueueItem extends StatelessWidget {
  const QueueItem({Key? key, required this.index, required this.song})
      : super(key: key);

  final Song song;
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        key: ValueKey(song),
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Queue.skipTo(index + 1);
        },
        child: Row(
          children: [
            Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  child: song.hasImage()
                      ? Image.memory(song.getImage(),
                          filterQuality: FilterQuality.high,
                          width: 64,
                          height: 64)
                      : const SizedBox(
                          width: 64,
                          height: 64,
                          child: DecoratedBox(
                              decoration: BoxDecoration(color: Colors.grey))),
                )),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 16,
                            child: Text(song.name,
                                maxLines: 1,
                                style: const TextStyle(
                                  inherit: false,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                ),
                                textAlign: TextAlign.left)),
                        Expanded(
                            flex: 14,
                            child: Text(song.interpret,
                                maxLines: 1,
                                style: const TextStyle(
                                    inherit: false,
                                    fontSize: 14,
                                    color: Colors.grey),
                                textAlign: TextAlign.left)),
                      ],
                    )),
              ),
            ),
            Align(
                alignment: Alignment.centerRight,
                child: ReorderableDragStartListener(
                    key: ValueKey(song),
                    index: index - 1,
                    child: const Padding(
                      padding: EdgeInsets.only(right: 25),
                      child: Icon(
                        Icons.reorder,
                        color: Colors.grey,
                      ),
                    )))
          ],
        ));
  }
}
