import 'package:flutter/material.dart';

import '../model/queue.dart';

class ActionBar extends StatelessWidget {
  const ActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: IconButton(
                  onPressed: () {
                    Queue.skipPrev();
                  },
                  icon: const Icon(Icons.skip_previous,
                      size: 50, color: Colors.grey))),
          Expanded(child: StatefulBuilder(builder: ((context, setState) {
            // rebuild when Queue.state is changed from the outside
            Queue.addStateChangeListener(() {
              setState(() {});
            });
            return IconButton(
                onPressed: () {
                  if (Queue.state == QueueState.playing) {
                    Queue.pause();
                  } else {
                    Queue.resume();
                  }
                },
                icon: Icon(
                    Queue.state == QueueState.playing
                        ? Icons.pause
                        : Icons.play_arrow,
                    size: 50,
                    color: Colors.grey[400]));
          }))),
          Expanded(
              child: IconButton(
                  onPressed: () {
                    Queue.skipNext();
                  },
                  icon: const Icon(Icons.skip_next,
                      size: 50, color: Colors.grey))),
        ],
      ),
    );
  }
}
