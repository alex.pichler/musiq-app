import 'package:flutter/material.dart';

import '../entities/song.dart';
import '../model/queue.dart';

class SongOverview extends StatefulWidget {
  const SongOverview({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SongOverviewState();
}

class _SongOverviewState extends State<SongOverview> {
  Song? _song = Queue.first();

  @override
  void initState() {
    Queue.addQueueChangeListener(() {
      setState(() {
        _song = Queue.first();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
      child: Column(children: [
        Expanded(
            flex: 5,
            child: StatefulBuilder(builder: ((context, setState) {
              return _song?.hasImage() ?? false
                  ? Image.memory(_song!.getImage(),
                      filterQuality: FilterQuality.high, fit: BoxFit.contain)
                  : const DecoratedBox(
                      decoration: BoxDecoration(color: Colors.grey));
            }))),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                _song?.name ?? "default",
                maxLines: 1,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey[300]),
              ),
              Text(_song?.interpret ?? "default",
                  maxLines: 1,
                  style: const TextStyle(fontSize: 16, color: Colors.grey)),
            ],
          ),
        )),
        const Expanded(child: SizedBox(height: 50))
      ]),
    );
  }
}
