// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'queue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FinishedSongRequest _$FinishedSongRequestFromJson(Map<String, dynamic> json) =>
    FinishedSongRequest();

Map<String, dynamic> _$FinishedSongRequestToJson(
        FinishedSongRequest instance) =>
    <String, dynamic>{};

FinishedSongResponse _$FinishedSongResponseFromJson(
        Map<String, dynamic> json) =>
    FinishedSongResponse(
      songId: json['songId'] as String,
    );

Map<String, dynamic> _$FinishedSongResponseToJson(
        FinishedSongResponse instance) =>
    <String, dynamic>{
      'songId': instance.songId,
    };

GetQueueRequest _$GetQueueRequestFromJson(Map<String, dynamic> json) =>
    GetQueueRequest();

Map<String, dynamic> _$GetQueueRequestToJson(GetQueueRequest instance) =>
    <String, dynamic>{};

GetQueueResponse _$GetQueueResponseFromJson(Map<String, dynamic> json) =>
    GetQueueResponse(
      queue: (json['queue'] as List<dynamic>)
          .map((e) => Song.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetQueueResponseToJson(GetQueueResponse instance) =>
    <String, dynamic>{
      'queue': instance.queue,
    };

QueueSongRequest _$QueueSongRequestFromJson(Map<String, dynamic> json) =>
    QueueSongRequest(
      link: json['link'] as String,
    );

Map<String, dynamic> _$QueueSongRequestToJson(QueueSongRequest instance) =>
    <String, dynamic>{
      'link': instance.link,
    };

QueueSongResponse _$QueueSongResponseFromJson(Map<String, dynamic> json) =>
    QueueSongResponse(
      id: json['id'] as String,
      name: json['name'] as String,
      link: json['link'] as String,
      interpret: json['interpret'] as String,
      imageData: json['imageData'] as String,
    );

Map<String, dynamic> _$QueueSongResponseToJson(QueueSongResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'link': instance.link,
      'interpret': instance.interpret,
      'imageData': instance.imageData,
    };

GetSongRequest _$GetSongRequestFromJson(Map<String, dynamic> json) =>
    GetSongRequest();

Map<String, dynamic> _$GetSongRequestToJson(GetSongRequest instance) =>
    <String, dynamic>{};

GetSongResponse _$GetSongResponseFromJson(Map<String, dynamic> json) =>
    GetSongResponse(
      audioData: json['song'] as String,
    );

Map<String, dynamic> _$GetSongResponseToJson(GetSongResponse instance) =>
    <String, dynamic>{
      'song': instance.audioData,
    };

ReorderSongRequest _$ReorderSongRequestFromJson(Map<String, dynamic> json) =>
    ReorderSongRequest(
      songId: json['songId'] as String,
      newSongIndex: json['newSongIndex'] as int,
    );

Map<String, dynamic> _$ReorderSongRequestToJson(ReorderSongRequest instance) =>
    <String, dynamic>{
      'songId': instance.songId,
      'newSongIndex': instance.newSongIndex,
    };

ReorderSongResponse _$ReorderSongResponseFromJson(Map<String, dynamic> json) =>
    ReorderSongResponse(
      songId: json['songId'] as String,
    );

Map<String, dynamic> _$ReorderSongResponseToJson(
        ReorderSongResponse instance) =>
    <String, dynamic>{
      'songId': instance.songId,
    };
