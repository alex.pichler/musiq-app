import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:musiq/client/api_client.dart';

import 'queue.dart';

part 'song.g.dart';

@JsonSerializable()
class Song {
  static const _maxRetries = 20;

  String id;
  String name;
  String interpret;
  String imageData;

  Uint8List? _image;
  Uint8List? _audio;

  Future<Uint8List> getAudio() async {
    if (_audio != null) return _audio!;

    await cacheAudio();

    return _audio != null ? Future.value(_audio!) : Future.error(Exception());
  }

  Future<void> cacheAudio() async {
    GetSongResponse? response;
    int retries = 0;

    // retry with increasing wait times while server is getting audio data
    while (retries < _maxRetries && response == null) {
      response = await APIClient.send(GetSongRequest(), arg: id);
      sleep(Duration(milliseconds: retries * 500));
      retries++;
    }

    if (retries == _maxRetries) return;

    _audio = base64Decode(response!.audioData);
  }

  void freeAudio() {
    _audio = null;
  }

  Song(
      {required this.id,
      required this.name,
      required this.interpret,
      required this.imageData,
      Uint8List? audio})
      : _audio = audio;

  Song.fromResponse(QueueSongResponse queueSongResponse)
      : id = queueSongResponse.id,
        name = queueSongResponse.name.trim(),
        interpret = queueSongResponse.interpret.trim(),
        imageData = queueSongResponse.imageData;

  @override
  bool operator ==(covariant Song other) => other.id == id;

  bool hasImage() => imageData != "";

  Uint8List getImage() => _image ??= base64Decode(imageData);

  factory Song.fromJson(Map<String, dynamic> json) => _$SongFromJson(json);

  Map<String, dynamic> toJson() => _$SongToJson(this);
}
