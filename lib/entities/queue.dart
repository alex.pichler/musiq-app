import 'package:musiq/entities/song.dart';
import 'package:json_annotation/json_annotation.dart';

part 'queue.g.dart';

@JsonSerializable()
class FinishedSongRequest {
  FinishedSongRequest();

  factory FinishedSongRequest.fromJson(Map<String, dynamic> json) =>
      _$FinishedSongRequestFromJson(json);

  Map<String, dynamic> toJson() => _$FinishedSongRequestToJson(this);
}

@JsonSerializable()
class FinishedSongResponse {
  String songId;

  FinishedSongResponse({required this.songId});

  factory FinishedSongResponse.fromJson(Map<String, dynamic> json) =>
      _$FinishedSongResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FinishedSongResponseToJson(this);
}

@JsonSerializable()
class GetQueueRequest {
  GetQueueRequest();

  factory GetQueueRequest.fromJson(Map<String, dynamic> json) =>
      _$GetQueueRequestFromJson(json);

  Map<String, dynamic> toJson() => _$GetQueueRequestToJson(this);
}

@JsonSerializable()
class GetQueueResponse {
  List<Song> queue;

  GetQueueResponse({required this.queue});

  factory GetQueueResponse.fromJson(Map<String, dynamic> json) =>
      _$GetQueueResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetQueueResponseToJson(this);
}

@JsonSerializable()
class QueueSongRequest {
  String link;

  QueueSongRequest({required this.link});

  factory QueueSongRequest.fromJson(Map<String, dynamic> json) =>
      _$QueueSongRequestFromJson(json);

  Map<String, dynamic> toJson() => _$QueueSongRequestToJson(this);
}

@JsonSerializable()
class QueueSongResponse {
  String id;
  String name;
  String link;
  String interpret;
  String imageData;

  QueueSongResponse(
      {required this.id,
      required this.name,
      required this.link,
      required this.interpret,
      required this.imageData});

  factory QueueSongResponse.fromJson(Map<String, dynamic> json) =>
      _$QueueSongResponseFromJson(json);

  Map<String, dynamic> toJson() => _$QueueSongResponseToJson(this);
}

@JsonSerializable()
class GetSongRequest {
  GetSongRequest();

  factory GetSongRequest.fromJson(Map<String, dynamic> json) =>
      _$GetSongRequestFromJson(json);

  Map<String, dynamic> toJson() => _$GetSongRequestToJson(this);
}

@JsonSerializable()
class GetSongResponse {
  @JsonKey(name: "song")
  String audioData;

  GetSongResponse({required this.audioData});

  factory GetSongResponse.fromJson(Map<String, dynamic> json) =>
      _$GetSongResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetSongResponseToJson(this);
}

@JsonSerializable()
class ReorderSongRequest {
  String songId;
  int newSongIndex;

  ReorderSongRequest({required this.songId, required this.newSongIndex});

  factory ReorderSongRequest.fromJson(Map<String, dynamic> json) =>
      _$ReorderSongRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ReorderSongRequestToJson(this);
}

@JsonSerializable()
class ReorderSongResponse {
  String songId;

  ReorderSongResponse({required this.songId});

  factory ReorderSongResponse.fromJson(Map<String, dynamic> json) =>
      _$ReorderSongResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ReorderSongResponseToJson(this);
}
