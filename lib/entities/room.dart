import 'package:musiq/entities/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'room.g.dart';

@JsonSerializable()
class Room {
  String id;
  String name;
  User owner;

  Room({required this.id, required this.name, required this.owner});

  Room.fromResponse(CreateRoomResponse createRoomResponse)
      : id = createRoomResponse.id,
        name = createRoomResponse.name,
        owner = createRoomResponse.owner;

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  Map<String, dynamic> toJson() => _$RoomToJson(this);
}

@JsonSerializable()
class CreateRoomRequest {
  String name;
  User owner;

  CreateRoomRequest({required this.name, required this.owner});

  factory CreateRoomRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateRoomRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CreateRoomRequestToJson(this);
}

@JsonSerializable()
class CreateRoomResponse {
  String id; //GUID
  String name;
  User owner;

  CreateRoomResponse(
      {required this.id, required this.name, required this.owner});

  factory CreateRoomResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateRoomResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CreateRoomResponseToJson(this);
}

@JsonSerializable()
class GetRoomsRequest {
  GetRoomsRequest();

  factory GetRoomsRequest.fromJson(Map<String, dynamic> json) =>
      _$GetRoomsRequestFromJson(json);

  Map<String, dynamic> toJson() => _$GetRoomsRequestToJson(this);
}

@JsonSerializable()
class GetRoomsResponse {
  List<Room> rooms;

  GetRoomsResponse({required this.rooms});

  factory GetRoomsResponse.fromJson(Map<String, dynamic> json) =>
      _$GetRoomsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetRoomsResponseToJson(this);
}

@JsonSerializable()
class JoinRoomRequest {
  User user;

  JoinRoomRequest({required this.user});

  factory JoinRoomRequest.fromJson(Map<String, dynamic> json) =>
      _$JoinRoomRequestFromJson(json);

  Map<String, dynamic> toJson() => _$JoinRoomRequestToJson(this);
}

@JsonSerializable()
class JoinRoomResponse {
  String roomId;

  JoinRoomResponse({required this.roomId});

  factory JoinRoomResponse.fromJson(Map<String, dynamic> json) =>
      _$JoinRoomResponseFromJson(json);

  Map<String, dynamic> toJson() => _$JoinRoomResponseToJson(this);
}
