import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:musiq/entities/room.dart';
import 'package:musiq/entities/user.dart';

import '../entities/queue.dart';

enum HttpMethod { get, post, put, delete, patch }

class APIClient {
  static const _apiVersion = "/v1";
  static final _endpoints = <Type, String>{
    CreateUserRequest: _apiVersion + "/user",
    GetRoomsRequest: _apiVersion + "/rooms",
    CreateRoomRequest: _apiVersion + "/room"
  };

  static const _methods = <Type, HttpMethod>{
    CreateUserRequest: HttpMethod.post,
    GetRoomsRequest: HttpMethod.get,
    CreateRoomRequest: HttpMethod.post,
    JoinRoomRequest: HttpMethod.patch,
    QueueSongRequest: HttpMethod.post,
    FinishedSongRequest: HttpMethod.patch,
    GetQueueRequest: HttpMethod.get,
    GetSongRequest: HttpMethod.get,
    ReorderSongRequest: HttpMethod.patch
  };

  static const _headers = <String, String>{
    "content-type": "application/json",
    "accept": "application/json"
  };

  static const String _userName = "default_user";
  static const String _roomName = "default_room";

  static final Uri _server = Uri.http("pixelforge.at:5501", "");

  static late Client _client;
  static late User _user;
  static late Room _room;

  static User get user => _user;
  static Room get room => _room;

  // private constructor
  APIClient._();

  static Future<void> init() async {
    var httpClient = HttpClient();
    // ignore invalid SSL certificates
    httpClient.badCertificateCallback = (cert, host, port) => true;
    _client = IOClient(httpClient);

    // no getUsers endpoint / users DB for now => create new user each time the app is opened
    _user = User.fromResponse(await send(CreateUserRequest(name: _userName)));

    GetRoomsResponse response = await send(GetRoomsRequest());

    Room? room;
    // check if room with default username exists
    for (var r in response.rooms) {
      if (r.owner.name == _userName) {
        room = r;
        break;
      }
    }
    // create new room if theres none
    _room = room ??
        Room.fromResponse(
            await send(CreateRoomRequest(name: _roomName, owner: _user)));

    // add endpoints containing our rooms id
    _endpoints.addAll(<Type, String>{
      JoinRoomRequest: _apiVersion + "/room/join/${_room.id}",
      QueueSongRequest: _apiVersion + "/queue/${_room.id}",
      FinishedSongRequest: _apiVersion + "/queue/${_room.id}",
      GetQueueRequest: _apiVersion + "/queue/${_room.id}",
      GetSongRequest: _apiVersion + "/queue/${_room.id}/", // needs songId
      ReorderSongRequest: _apiVersion + "/queue/${_room.id}"
    });

    // if we didnt create the room, join it
    if (room != null) {
      await send(JoinRoomRequest(user: _user));
    }
  }

  /// Method for sending every possible request to the API. Automatically constructs the appropriate response,
  /// i.e. a GetQueueResponse for a GetQueueRequest.
  ///
  /// For a GetSongRequest, the songId must be passed in arg.
  static Future send<T>(T request, {String arg = ""}) async {
    if (T == GetSongRequest && arg == "") {
      throw ArgumentError("Cant send GetSongRequest without songId", "arg");
    }

    var uri = _server.resolve(_endpoints[T]! + arg);
    var httpReq = Request(_methods[T]!.name, uri);

    if (_requiresJson(T)) {
      httpReq.headers.addAll(_headers);
      httpReq.body = jsonEncode(request);
    }

    var httpRes = await _client.send(httpReq);

    if (httpRes.statusCode < 200 || httpRes.statusCode >= 300) {
      throw HttpException("$T failed", uri: uri);
    }

    Map<String, dynamic> jsonMap =
        jsonDecode(await httpRes.stream.bytesToString());

    switch (T) {
      case CreateUserRequest:
        return CreateUserResponse.fromJson(jsonMap);
      case CreateRoomRequest:
        return CreateRoomResponse.fromJson(jsonMap);
      case GetRoomsRequest:
        return GetRoomsResponse.fromJson(jsonMap);
      case QueueSongRequest:
        return QueueSongResponse.fromJson(jsonMap);
      case GetSongRequest:
        return !jsonMap.values.contains(null)
            ? GetSongResponse.fromJson(
                jsonMap) // API responds with null if the audio data is not downloaded yet -> dont create response object
            : null;
      case FinishedSongRequest:
        return FinishedSongResponse.fromJson(jsonMap);
      case GetQueueRequest:
        return GetQueueResponse.fromJson(jsonMap);
      case ReorderSongRequest:
        return ReorderSongResponse.fromJson(jsonMap);
      case JoinRoomRequest:
        return JoinRoomResponse.fromJson(jsonMap);
      default:
        throw TypeError();
    }
  }

  static bool _requiresJson(Type t) {
    return [
      GetQueueRequest,
      ReorderSongRequest,
      QueueSongRequest,
      CreateRoomRequest,
      JoinRoomRequest,
      CreateUserRequest
    ].contains(t);
  }
}
