import 'dart:async';

import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

enum MessageType { play, pause, skipPrev, skipNext, skipTo, queueUpdate }

class MessageClient {
  static const _server = "pixelforge.at";
  static const _port = 9001;
  static const _separator = " ";

  static late StreamSubscription _publishedStreamSubscription;
  static late MqttServerClient _client;
  static late String _clientId;
  static late String _topic;

  static final _listeners = <MessageType, Function>{};

  MessageClient._();

  static void init(String userId, String? roomId) {
    _clientId = userId;
    _topic = roomId ?? "debug";

    _client = MqttServerClient.withPort(_server, _clientId, _port);

    _client.setProtocolV311();
    _client.logging(on: false);
    _client.keepAlivePeriod = 5;
    _client.autoReconnect = false;
    _client.onBadCertificate = (dynamic a) => true;

    _client.connectionMessage = MqttConnectMessage()
        .withClientIdentifier(_clientId)
        .startClean(); // non persistent session as we dont want old messages

    _client.onConnected = () {
      if (_client.connectionStatus!.state == MqttConnectionState.connected) {
        _client.subscribe(_topic, MqttQos.exactlyOnce);
        _publishedStreamSubscription = _client.updates!.listen((messages) {
          for (var mqttMessage in messages) {
            final pubMessage = mqttMessage.payload as MqttPublishMessage;
            final payload = MqttPublishPayload.bytesToStringAsString(
                pubMessage.payload.message);

            // only respond to event if it was sent from someone else
            if (!payload.contains(_clientId)) {
              String cmd = payload.split(_separator)[1];
              if (MessageType.values.any((type) => type.name == cmd)) {
                MessageType type = MessageType.values.byName(cmd);
                _listeners[type]!.call();
              }
            }
          }
        });
      }
    };

    _client.connect();
  }

  /// Publishes an event (clientId + type)
  static void publish(MessageType type) {
    if (_client.connectionStatus!.state == MqttConnectionState.connected) {
      _client.publishMessage(
          _topic,
          MqttQos.exactlyOnce,
          MqttClientPayloadBuilder()
              .addString(_clientId)
              .addString(_separator)
              .addString(type.name)
              .payload!);
    }
  }

  static void addListener(MessageType type, Function listener) {
    _listeners[type] = listener;
  }

  static void removeListener(MessageType type, Function listener) {
    _listeners.remove(type);
  }

  static void dispose() {
    _listeners.clear();
    _publishedStreamSubscription.cancel();
    _client.unsubscribe(_topic);
    _client.disconnect();
  }
}
