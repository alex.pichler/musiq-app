import 'dart:math';

import 'package:flutter/material.dart';
import 'package:musiq/model/queue.dart';
import 'package:musiq/widgets/queue_item.dart';
import 'package:musiq/widgets/song_overview.dart';

import '../widgets/action_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
        decoration: const BoxDecoration(color: Color.fromARGB(255, 12, 64, 88)),
        child: CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            const SliverAppBar(
                pinned: true,
                snap: true,
                floating: true,
                expandedHeight: 450.0,
                flexibleSpace: DecoratedBox(
                  decoration:
                      BoxDecoration(color: Color.fromARGB(255, 6, 38, 51)),
                  child: FlexibleSpaceBar(
                      centerTitle: true,
                      title: ActionBar(),
                      background: SongOverview()),
                )),
            SliverFillRemaining(
              child: StatefulBuilder(builder: ((context, setState) {
                Queue.addQueueChangeListener(() {
                  setState(() {});
                });
                return ReorderableList(
                  controller: _scrollController,
                  itemExtent: 74.0,
                  itemBuilder: (BuildContext context, int index) {
                    var song = Queue.get(index + 1);
                    return QueueItem(
                        key: ValueKey(song), song: song!, index: index + 1);
                  },
                  itemCount: max(0, Queue.size() - 1),
                  onReorder: (int oldIndex, int newIndex) =>
                      Queue.reorder(oldIndex + 1, newIndex + 1),
                );
              })),
            ),
          ],
        ));
  }
}
