import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:musiq/client/api_client.dart';
import 'package:musiq/client/message_client.dart';

import '../entities/queue.dart';
import '../entities/song.dart';

enum QueueState { playing, paused, waiting, loading }

class Queue {
  static QueueState _state = QueueState.waiting;
  static AudioPlayer? _audioPlayer;
  static final List<Song> _list = List.empty(growable: true);

  static StreamSubscription? _songFinishedSubscription;
  static final List<Function> _stateChangeListeners =
      List.empty(growable: true);
  static final List<Function> _queueChangeListeners =
      List.empty(growable: true);

  static QueueState get state => _state;

  /// Setter for state which invokes updateState() in the background
  static set state(QueueState value) {
    _state = value;
    updateState();
  }

  // private constructor
  Queue._();

  static init(AudioPlayer audioPlayer) {
    _audioPlayer = audioPlayer;
    _songFinishedSubscription = _audioPlayer?.onPlayerCompletion.listen((_) {
      skipNext();
      return;
    });

    /// register MQTT listeners (dispatchEvent: false because we dont want to
    /// re-broadcast events that every other client should have already received)
    MessageClient.addListener(
        MessageType.play, () => resume(dispatchEvent: false));
    MessageClient.addListener(
        MessageType.pause, () => pause(dispatchEvent: false));
    MessageClient.addListener(
        MessageType.skipPrev, () => skipPrev(dispatchEvent: false));
    MessageClient.addListener(
        MessageType.skipNext, () => skipNext(dispatchEvent: false));
    MessageClient.addListener(
        MessageType.skipTo, () => pullQueue(dispatchEvent: false));
    MessageClient.addListener(
        MessageType.queueUpdate, () => pullQueue(dispatchEvent: false));

    _list.clear();
    pullQueue();
  }

  static Song? first() => get(0);

  static Song? get(int index) => index < _list.length ? _list[index] : null;

  static void add(String songLink) => insert(_list.length, songLink);

  /// Gets a song from a given link from the server and inserts it into the list
  static void insert(int index, String songLink) async {
    if (!songLink.contains("youtube")) return;

    // map yt music link to yt link
    if (songLink.contains("music.youtube.com")) {
      songLink = songLink.replaceFirst("music.", "");
      songLink = songLink.replaceRange(songLink.indexOf("&"), null, "");
    }

    try {
      QueueSongResponse response =
          await APIClient.send(QueueSongRequest(link: songLink));
      Song song = Song.fromResponse(response);
      _list.insert(index, song);
    } catch (e) {
      return;
    }

    updateUI();
    MessageClient.publish(MessageType.queueUpdate);
  }

  static int size() => _list.length;

  /// Moves a song from oldIndex to newIndex
  static void reorder(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      // removing the song will shorten the list by 1
      newIndex -= 1;
    }
    final Song song = _list.removeAt(oldIndex);
    _list.insert(newIndex, song);
    APIClient.send(ReorderSongRequest(songId: song.id, newSongIndex: newIndex));
    updateUI();
    MessageClient.publish(MessageType.queueUpdate);
  }

  /// Pulls the entire Queue from the server
  static void pullQueue({bool dispatchEvent = true}) {
    APIClient.send(GetQueueRequest()).then((response) {
      List<Song> newList = (response as GetQueueResponse).queue;
      QueueState prevState = state;
      if (newList.isEmpty || newList[0] != first()) {
        state = QueueState.waiting;
        _audioPlayer?.stop();
        _audioPlayer?.release();
      }

      _list.clear();
      _list.addAll(newList);

      if (prevState == QueueState.playing && state == QueueState.waiting) {
        resume(dispatchEvent: false);
      }

      updateUI();
    });
  }

  static void updateUI() {
    _queueChangeListeners.forEach((listener) => listener.call());
  }

  static void updateState() {
    _stateChangeListeners.forEach((listener) => listener.call());
  }

  /// Resumes the current song where it was paused, or starts the first song in the queue
  static void resume({bool dispatchEvent = true}) {
    switch (state) {
      case QueueState.playing:
        return;
      case QueueState.waiting:
        first()?.getAudio().then((bytes) {
          _audioPlayer?.playBytes(bytes, respectSilence: true, stayAwake: true);
          return null;
        }).onError((error, stackTrace) {
          state = QueueState.waiting;
        });

        // start caching audio for next song in the background to avoid blocking
        get(1)?.cacheAudio();
        break;
      case QueueState.paused:
        _audioPlayer?.resume();
        break;
      default:
    }

    if (first() != null) {
      state = QueueState.playing;
      if (dispatchEvent) MessageClient.publish(MessageType.play);
    }
  }

  /// Pauses the current song
  static void pause({bool dispatchEvent = true}) {
    if (state != QueueState.playing) return;

    state = QueueState.paused;
    if (dispatchEvent) MessageClient.publish(MessageType.pause);
    _audioPlayer?.pause();
  }

  /// Skips to the next song in the queue
  static void skipNext({bool dispatchEvent = true}) {
    skipTo(1, dispatchEvent: dispatchEvent);
  }

  /// Skips to the beginning of the current song
  static void skipPrev({bool dispatchEvent = true}) {
    if (first() == null || state == QueueState.waiting) return;

    _audioPlayer?.seek(Duration.zero);
    if (dispatchEvent) MessageClient.publish(MessageType.skipPrev);
  }

  /// Skips to the song at index, starts playing if another song is playing while calling this method
  static void skipTo(int index, {bool dispatchEvent = true}) {
    if (index > _list.length) return;

    _list.removeRange(0, index);

    if (_list.isEmpty) {
      _audioPlayer?.stop();
      _audioPlayer?.release();
      state = QueueState.waiting;
    }

    for (var i = 0; i < index; i++) {
      APIClient.send(FinishedSongRequest());
    }

    updateUI();
    if (dispatchEvent) MessageClient.publish(MessageType.queueUpdate);

    if (state == QueueState.paused) {
      _audioPlayer?.release();
      state = QueueState.waiting;
    } else if (state == QueueState.playing) {
      _audioPlayer?.stop();
      _audioPlayer?.release();
      first()?.getAudio().then((bytes) {
        _audioPlayer?.playBytes(bytes, respectSilence: true, stayAwake: true);
        return null;
      }).onError((error, stackTrace) {
        state = QueueState.waiting;
      });
    }

    // start caching audio for next song in the background to avoid blocking
    get(1)?.cacheAudio();
  }

  static void addStateChangeListener(Function listener) {
    _stateChangeListeners.add(listener);
  }

  static void addQueueChangeListener(Function listener) {
    _queueChangeListeners.add(listener);
  }

  static void dispose() {
    _songFinishedSubscription?.cancel();
    _stateChangeListeners.clear();
    _queueChangeListeners.clear();
    _list.clear();
    _state = QueueState.waiting;
  }
}
