import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:musiq/client/api_client.dart';
import 'dart:async';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:perfect_volume_control/perfect_volume_control.dart';

import 'client/message_client.dart';
import 'model/queue.dart';
import 'screens/home.dart';

void main() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  late StreamSubscription _intentDataStreamSubscription;
  String? _sharedText;
  late AudioPlayer audioPlayer;

  @override
  void initState() {
    super.initState();

    audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    audioPlayer.setReleaseMode(ReleaseMode.STOP);

    APIClient.init().then((_) {
      // init MessageClient and Queue after APIClient as they require a userId and roomId
      MessageClient.init(APIClient.user.id, APIClient.room.id);
      Queue.init(audioPlayer);
    });

    PerfectVolumeControl.getVolume()
        .then((volume) => audioPlayer.setVolume(volume));

    PerfectVolumeControl.stream
        .listen((double volume) => audioPlayer.setVolume(volume));

    // Receive shared URLs while app is in memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
      setState(() {
        _sharedText = value;
        Queue.add(_sharedText!);
      });
    });

    // Receive shared URLs while app is closed
    ReceiveSharingIntent.getInitialText().then((String? value) {
      if (value != null) {
        setState(() {
          _sharedText = value;
          Queue.add(_sharedText!);
        });
      }
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    Queue.dispose();
    MessageClient.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MusiQ',
      theme: ThemeData(useMaterial3: true),
      home: const HomePage(),
    );
  }
}
