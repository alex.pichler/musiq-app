# MusiQ App

Eine App, die von mehreren Personen verwendet werden kann, um Musik von verschiedenen Musik-Apps in eine gemeinsame Queue hinzuzufügen und diese Queue zu verwalten. In dieser App erstellt einer der Benutzer einen Raum, dem dann andere Personen beitreten können. Diese Räume und die Musik-Queue werden zentral von einem Server gespeichert, also geht die Queue auch nicht verloren, wenn jemand aus Versehen die App schließt oder die Internetverbindung verliert.

## Verwendung

Um MusiQ zu testen, oder zu verwenden, wird entweder ein Android-Gerät, oder ein Android-Emulator (Android Studio) benötigt. (https://www.alphr.com/run-android-emulator/)
Sollte ein echtes Gerät verwendet werden, muss dafür Entwicklermodus eingeschalten werden, und USB-Debuggung aktiviert sein. Die Verbindung erfolgt über USB.

Es wird beim Start der App automatisch eine Verbindung zum Server aufgebaut, dieser läuft innerhalb der nächsten Monate weiter. Sollte die Verbindung zum Server fehlschlagen, kontaktieren Sie Fabian Bleck.
